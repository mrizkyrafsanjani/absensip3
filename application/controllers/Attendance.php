<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {

	public $crud;
	public $module;

	public function __construct(){
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('Encrypt');
		$this->load->library(('ciqrcode'));
		$this->crud = new grocery_CRUD();
		$this->crud->set_theme('datatables');
		$this->module = 'participant';			
	}

	public function index()
	{		
		$this->crud->set_table($this->module);
		//P3
		/*
		$this->crud->columns('NoMeja','Perusahaan', 'KdUnitUsaha','KdPeserta','NmPeserta', 'NPK', 'KTP', 'Kehadiran');
		$this->crud->field_type('Kehadiran','dropdown',
		array('0' => 'belum hadir', '1' => 'Hadir'));
		$this->crud->field_type('KTP','dropdown',
		array('0' => 'belum KTP', '1' => 'KTP'));
		$this->crud->field_type('NPWP','dropdown',
		array('0' => 'belum NPWP', '1' => 'NPWP'));
		$this->crud->display_as('KdPeserta','Nomor Peserta')->display_as('NmPeserta','Nama Peserta')->display_as('KdUnitUsaha','Kode Mitra')->display_as('NPK','NIP');;
		$this->crud->add_action('Hadir', '', '','ui-icon-check',array($this,'ajax_hadir'));
		$this->crud->add_action('KTP', '', '','ui-icon-check',array($this,'ajax_KTP'));
		*/
		//DIC Gathering
		$this->crud->columns('NoMeja','KdMitra','NoPeserta','NIP','NmPeserta','Perusahaan','DPA','NoHP', 'Kehadiran');
		$this->crud->field_type('Kehadiran','dropdown',
		array('0' => 'belum hadir', '1' => 'Hadir'));
		$this->crud->display_as('NoPeserta','Nomor Peserta')->display_as('NmPeserta','Nama Peserta')->display_as('KdUnitUsaha','Kode Mitra')->display_as('NoMeja','Nomor Meja')->display_as('KdMitra','Kode Mitra')->display_as('NoHP','Nomor HP');
		$this->crud->add_action('WA', '', '','ui-icon-check',array($this,'ajax_WA'));
		$this->crud->add_action('Hadir', '', '','ui-icon-check',array($this,'ajax_hadir'));
		$this->crud->unset_read();
		$this->crud->unset_delete();
		// $this->crud->add_action('NPWP', '', '','ui-icon-check',array($this,'ajax_NPWP'));
		//$this->crud->callback_column('Kehadiran',array($this,'_callbackKehadiran'));
		$output = $this->crud->render();
		$count = "SELECT count(0) as TotalHadir from participant where kehadiran = 1";
		$totalHadir = $this->db->query($count)->result();
		$count = "SELECT count(0) as TotalPeserta from participant";
		$totalPeserta = $this->db->query($count)->result();
		$other = array(
			$totalHadir[0],
			$totalPeserta[0]
		);
		$this->mytemplate->loadAmin('attendance',$output, false, $other);
	}

	public function resetkehadiran(){

		$sql = "UPDATE participant set Kehadiran = 0 ";
		$this->db->query($sql);
		echo json_encode(array("message"=>'Sukses reset'));
		redirect('attendance','refresh');
	}
	public function setHadir($id, $Kehadiran){

		if($Kehadiran == "belum%20hadir"){
			$sql = "UPDATE participant set Kehadiran = 1 where id = '$id'";
		}else{
			$sql = "UPDATE participant set Kehadiran = 0 where id = '$id'";
		}
		$this->db->query($sql);
		redirect('attendance','refresh');
	}
	public function setKTP($id, $KTP){

		if($KTP == "belum%20KTP"){
			$sql = "UPDATE participant set KTP = 1 where id = '$id'";
		}else{
			$sql = "UPDATE participant set KTP = 0 where id = '$id'";
		}
		$this->db->query($sql);
		redirect('attendance','refresh');
	}
	public function setNPWP($id, $NPWP){

		if($NPWP == "belum%20NPWP"){
			$sql = "UPDATE participant set NPWP = 1 where id = '$id'";
		}else{
			$sql = "UPDATE participant set NPWP = 0 where id = '$id'";
		}
		$this->db->query($sql);
		redirect('attendance','refresh');
	}

	function ajax_hadir($primary_key, $row){
		return site_url("attendance/setHadir/".$row->id."/".$row->Kehadiran);
	}
	function ajax_KTP($primary_key, $row){
		return site_url("attendance/setKTP/".$row->id."/".$row->KTP);
	}
	function ajax_NPWP($primary_key, $row){
		return site_url("attendance/setNPWP/".$row->id."/".$row->NPWP);
	}
	function ajax_WA($primary_key, $row){
		return site_url("attendance/hubungi_peserta/".$row->id);
	}

	function _callbackKehadiran($value, $row){
		
		if($value == 0){
			return "Belum Hadir";
		}else{
			return "Hadir";
		}
	}
	function _callbackKTP($value, $row){
		
		if($value == 0){
			return "Belum KTP";
		}else{
			return "KTP";
		}
	}
	function _callbackNPWP($value, $row){
		
		if($value == 0){
			return "Belum NPWP";
		}else{
			return "NPWP";
		}
	}

	//untuk QR Code
	function myQRCode(){
		$qrcode = $_POST['qrcode'];

		$myqrcode = $this->decode($qrcode);
		$sqlCheck = "SELECT * from participant where NoPeserta = '$myqrcode';";
		$checkData = $this->db->query($sqlCheck);
		$checkDataJson = json_encode($checkData->result());

		
		if($checkData->num_rows()>0){
			
			$sqlAttend = "UPDATE participant set Kehadiran = 1 where NoPeserta = '$myqrcode';";
			$result = $this->db->query($sqlAttend);
			header("Content-Type: application/json");
			header("Access-Control-Allow-Origin: *");
			header("Access-Control-Allow-Headers: *");
			echo json_encode($checkData->result());
			
		}else{
			return false;
		}
	}

	function getQRData($qrcode){
		// $qrcode = $_GET['qrcode'];
		$qrcode_DEC = $this->decode($qrcode);
		$sql = "SELECT * FROM participant where NoPeserta = $qrcode_DEC;";
		$query = $this->db->query($sql);
		header("Content-Type: application/json");
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		echo json_encode($query->result());
		// return $data;
		
	}

	function decode($qrcode){
		$decParam = strtr(
			$qrcode,
			array(
				'.' => '+',
				'-' => '=',
				'~' => '/'
			)
		);
		// echo $this->encrypt->decode($decParam);
		return $this->encrypt->decode($decParam);
	}

	function encode($KdPeserta){
		$ret = $this->encrypt->encode($KdPeserta);

				if (!empty($KdPeserta))
				{
					$ret = strtr(
							$ret,
							array(
								'+' => '.',
								'=' => '-',
								'/' => '~'
							)
						);
				}
				echo $ret;
				return $ret;
	}

	function generateQRCode(){
		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/qrimages/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)

		$sqlSelect = "SELECT * FROM participant;";
		$query = $this->db->query($sqlSelect);
		$jumlah = 0;
		foreach($query->result() as $row){
			$noPeserta = $row->NoPeserta;
			$noPeserta_Enc = $this->encode($noPeserta);
			$NmPeserta = $row->NmPeserta;
			$Perusahaan = $row->Perusahaan;
			$NoMeja = $row->NoMeja;
			echo "Begin QR Code";
			
			$this->ciqrcode->initialize($config);
			$image_name=str_replace(' ','',$Perusahaan).'_'.str_replace(' ','',$NmPeserta).'_'.str_replace(' ','',$noPeserta).'.png'; //buat name dari qr code sesuai dengan Perusahaan_Nama_NoPes
			// $params['data'] = $noPeserta."_".$NmPeserta."_".$NoMeja; //data yang akan di jadikan QR CODE
			$params['data'] = $noPeserta_Enc; //data yang akan di jadikan QR CODE
			$params['level'] = 'H'; //H=High ; M=
			$params['size'] = 10;
			$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
			$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			$jumlah++;
			echo "create ".$jumlah."/".$query->num_rows()." Create QR Code Done. ";

		}
	}

	function hubungi_peserta($id){
		$sql = "SELECT NoPeserta, NmPeserta, Perusahaan, substring_index(case when NoHP like '0%' then substring(NoHP,2)
		when NoHP like '+62%' then substring(NOHP,4)
		else NoHP end,'/',1) NoHP from participant
		where id = ?";
		$query = $this->db->query($sql, $id);

		foreach($query->result() as $row){
			$phoneNumber  = "62".str_replace(" ","",$row->NoHP);
			// $phoneNumber = "6282328896395";
			$files = str_replace(' ','',$row->Perusahaan)."_".str_replace(' ','',$row->NmPeserta)."_".str_replace(' ','',$row->NoPeserta);
			// echo $files." No HP : ".$phoneNumber;

			$defaultMessage = "Yth+Bapak%2FIbu%2C%0D%0ABerikut+QR+code+yang+dapat+Bapak%2FIbu+tunjukkan+pada+saat+registrasi+pada+P3+Tahap+I+Angkatan+47+tanggal+10-11+Maret+2020.+Mohon+klik+link+terlampir.+Terima+kasih.%0D%0A%0D%0Ahttps%3A%2F%2Fdapenastra.com%2Fdpadiginew%2Fassets%2Fqrimages%2F$files.png";
		}
		redirect('https://web.whatsapp.com/send?phone='.$phoneNumber.'&text='.$defaultMessage,'title="Hubungi Peserta"');
	}
}
