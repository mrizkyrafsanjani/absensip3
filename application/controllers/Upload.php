<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public $crud;
	public $module;

	public function __construct(){
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->crud = new grocery_CRUD();
		$this->crud->set_theme('datatables');
		$this->module = 'participant';
		//load the excel library
		//$this->load->library('excel');			
	}

	public function index()
	{		
		$this->mytemplate->loadAmin('upload');
	}
	public function do_upload(){
				
		require 'application/third_party/PHPExcel/IOFactory.php';
		try
		{
			$filename=($_FILES['fileToUpload']['name']);
			$tempfile=$_FILES['fileToUpload']['tmp_name'];
			$inputfiletype = PHPExcel_IOFactory::identify($tempfile);
			$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
			$counterTable = 1;
			$uploadOk = 1;
			$msg = "";
			$createdon = date("Y-m-d h:i:s");
			if(isset($_POST["submit"])) {
				if ($_FILES["fileToUpload"]["size"] > 500000) {
					$msg = "Maaf, size file yang di kirimkan terlalu besar";
					$uploadOk = 0;
				}
				if(strrpos($inputfiletype, "Excel5") == -1 && $inputfiletype != "xls" && $inputfiletype != "xlsx" && $inputfiletype != "application/vnd.ms-excel") {
					$msg = "Maaf, hanya tipe file xls dan xlsx yang di izinkan";
					$uploadOk = 0;
				}
				if ($uploadOk == 0) {
					$this->mytemplate->loadAmin('upload', $msg);
				} else {
					$objPHPExcel = $objReader->load($tempfile);
					$sheet = $objPHPExcel->getSheet(0); 
					$highestRow = $sheet->getHighestRow(); 
					$highestColumn = 'I';
					$sql = "DELETE FROM participant";
					$this->db->query($sql);
					for ($row = 2; $row <= $highestRow; $row++)
					{ 
						//  Read a row of data into an array
						$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
						$exceldata = $rowData[0];
						if($exceldata[0] != ""){
							$TanggalLahir = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($exceldata[6]));
						
							$sql = "INSERT into participant (id, 
											NoMeja, 
											KdMitra, 
											NoPeserta, 
											NIP, 
											NmPeserta, 
											Perusahaan, 
											TanggalLahir, 
											DPA,
											NoHP)
							VALUES ($counterTable, '$exceldata[0]','$exceldata[1]','$exceldata[2]','$exceldata[3]','$exceldata[4]','$exceldata[5]','$TanggalLahir','$exceldata[7]','$exceldata[8]');";
							$this->db->query($sql);
							$counterTable++;
							
						}
					}
					$msg = "Sukses Upload ".$filename;
					$data = array(
						'title'=> 'Data Upload',
						'msg' => $msg
					);
					$this->mytemplate->loadAmin('Upload', $data);
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );				
		}
	}
}
