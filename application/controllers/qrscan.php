<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Qrscan extends CI_Controller {
    public $crud;
	public $module;

	public function __construct(){
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->crud = new grocery_CRUD();
		$this->crud->set_theme('datatables');
		$this->module = 'participant';
		//load the excel library
		//$this->load->library('excel');			
	}
    public function index()
	{		
		$this->load->view('qrscan_view');
	}
}?>