<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Attendance</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'/assets/css/bootstrap.min.css';?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'/assets/css/sb-admin.css';?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url().'/assets/css/plugins/morris.css';?>" rel="stylesheet">
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'/assets/font-awesome/css/font-awesome.min.css';?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        function resetKehadiran(){
            var answer = prompt("Masukkan password reset");
            if(answer == 'dapenastra'){
                $('#divLoadingSubmit').show();
                $.ajax({
                    url: '<?php echo $this->config->base_url(); ?>attendance/resetkehadiran',
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    cache: false,
                    success: function(data)
                    {
                        console.log(data);
                        alert(data.message);
                        $('#divLoadingSubmit').hide();
                        location.reload();
                    },
                    error: function (request, status, error) {
                        console.log(error);
                        alert('Error');
                    }
                });
            }else{
                event.preventDefault();
            }
        }
    </script>
</head>

<body>

    <div id="wrapper">
    <div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url().'attendance';?>">Attendance</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Dana Pensiun Astra <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a onclick="resetKehadiran()" href = "#"><i class="fa fa-fw fa-gear"></i> Reset Kehadiran</a>
                        </li>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <!-- <li class="active">                        
						<?php echo anchor(base_url().'welcome','<i class="fa fa-fw fa-dashboard"></i> Dashboard');?>
                    </li> -->
                    <li>
                        <a href="<?php echo base_url().'attendance';?>"><i class="fa fa-fw fa-edit"></i> Attendance</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'upload';?>"><i class="fa fa-fw fa-upload"></i> Upload Data</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <?php if(!empty($other) && $other[1]->TotalPeserta>0): ?>
                <?php echo 'Peserta: '.$other[0]->TotalHadir.' dari '.$other[1]->TotalPeserta.' | '. round((float)($other[0]->TotalHadir/$other[1]->TotalPeserta)* 100, 2 ) . '%';?>
            <?php endif;?>
            <hr/>
            <?php echo $contents;?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'/assets/js/jquery.js';?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'/assets/js/bootstrap.min.js';?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url().'/assets/js/plugins/morris/raphael.min.js';?>"></script>
    <script src="<?php echo base_url().'/assets/js/plugins/morris/morris.min.js';?>"></script>
    <script src="<?php echo base_url().'/assets/js/plugins/morris/morris-data.js';?>"></script> 
    <?php    
if(isset($css_files)):    
        foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php foreach($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
    <?php endforeach;
endif;
?>   
</body>

</html>
