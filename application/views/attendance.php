 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Kehadiran <small>DIC Gathering <?php echo date("d-m-Y");?></small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Dana Pensiun Astra
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <?php echo $output;?>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
            