 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Upload <small>Data Kehadiran DIC Gathering</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Dana Pensiun Astra
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php if (!empty($msg)): ?>
                <span class="label label-danger"><?php echo $msg;?></span>
                <?php endif;?>
            <hr/>
        </div>
    </div> 
    <div class="row">
        <div class="col-lg-12">
                <small>Mohon upload dengan format berikut, kosongkan yang tidak perlu <b>(Beserta Header)</b> <a href="<?php echo site_url().'downloads/Template.xlsx';?>" download> download template</a></small>
        <table style="width:100%" class="table table-striped">
                <tr>
                    <td>Nomor Meja</td>
                    <td>Kode Mitra</td>
                    <td>Nomor Peserta</td>
                    <td>NIP</td>
                    <td>Nama Peserta</td>
                    <td>Perusahaan</td>
                    <td>Tanggal Lahir</td>
                    <td>DPA</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>001</td>
                    <td>999999</td>
                    <td>080-17</td>
                    <td>Gerry Gabriel Wijaya</td>
                    <td>Astra International</td>
                    <td>Bulan/Tanggal/Tahun</td>
                    <td>DPA 2</td>
                </tr>
            </table><br>

        <form action = "<?php echo base_url().'upload/do_upload';?>" method="post" enctype="multipart/form-data">
            <table style="width:100%">
                <tr>
                    <td height="40" style="width:20%">Pilih File untuk di upload</td>
                    <td width="10px"><input class="form-control" type="file" name="fileToUpload" id="fileToUpload"></td>
                </tr>
            </table><br>
           
            <div class="form-actions">
                <input type="submit" id="btnUpload" class="btn btn-success" value="Upload xls" name="submit">
                <a class="btn btn-default" href="hasil_upload.php">Back</a>
            </div>
            </form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
            